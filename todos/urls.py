from django.urls import path
from . import views

app_name='todos'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:todo_id>/delete', views.delete, name='delete'),
    path('<int:todo_id>/update', views.update, name='update'),
    path('add/', views.add, name='add'),
    path('addEvent/', views.add_event, name='addEvent'),
    path('event/delete/<int:event_id>/', views.delete_event, name='deleteEvent'),
]