from django import forms
from .models import Event

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ('event_name', 'description', 'status', 'event_date', 'user')